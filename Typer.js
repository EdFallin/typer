/**/

export class Typer {
    content =
        "When you type over this, the letters are matched one by one as they overlay the original, "
        + "while the coloring is adjusted on the existing text using spans. The result is "
        + "that the text's appearance responds to whether you are typing correctly or not.";

    ignorables = [
        "Shift", "Control", "Command", "Option", "Alt", "Meta",
        "ArrowLeft", "ArrowRight", "ArrowUp", "ArrowDown"
    ];

    reversers = ["Backspace", "Delete"];

    at = -1;

    constructor(dom) {
        this.dom = dom;

        this.stencil = this.dom.getElementById("Stencil");
        this.text = this.dom.getElementById("Text");
        this.resetter = this.dom.getElementById("Resetter");
    }

    run() {
        // Initing the text and UI interactions.
        this.asSpans();
        this.wire();
    }

    asSpans() {
        // Adding all the text with invisible spans.
        for (let char of this.content) {
            let span = this.stencil.cloneNode(false);
            span.innerText = char;
            this.text.appendChild(span);
        }

        // Making the first character look ready to type.
        this.styleNext();
    }

    wire() {
        this.text.addEventListener("keydown", (e) => {
            this.whenKeyDown(e);
        });

        this.text.addEventListener("keyup", (e) => {
            this.whenKeyUp(e);
        });

        this.resetter.addEventListener("click", (e) => {
            this.reset();
        });
    }

    // Used to block moving using the arrow keys.
    whenKeyDown(e) {
        // Ignoring keys other than arrows.
        if (!this.ignorables.includes(e.key)) {
            return;
        }

        // Arrowed movement is prevented.
        this.skipHandling(e);
    }

    // Used ot handle all keys, whether characters or otherwise.
    whenKeyUp(e) {
        // Keys only appear to be typed over.
        this.skipHandling(e);

        // Movement and altering keys are ignored.
        if (this.ignorables.includes(e.key)) {
            return;
        }

        // Deleting is imitated.
        if (this.reversers.includes(e.key)) {
            // You can't delete to before the start.
            this.at = Math.max(-1, this.at -1);

            // Styling next character and unstyling the one after.
            this.styleNextChars();
            return;
        }

        /* Regular typing is compared with the original. */

        this.at++;

        // The span for the current letter is localized and
        // its styling removed so it's ready for restyling.
        let at = this.text.children[this.at];
        at.classList.remove("untyped", "next", "good", "bad");

        // The current span is styled based on the typing.
        let styling = "outre";

        if (e.key === this.content[this.at]) {
            styling = "good";
        }
        else {
            styling = "bad";
        }

        at.classList.add(styling);

        this.styleNextChars();
    }

    skipHandling(e) {
        e.preventDefault();
        e.stopPropagation();
    }

    styleNextChars() {
        this.styleNext();
        this.styleNextAfter();
    }

    styleNext() {
        let next = this.text.childNodes[this.at +1];
        next.classList.remove("untyped", "next", "good", "bad");
        next.classList.add("next");
    }

    styleNextAfter() {
        let next = this.text.childNodes[this.at +2];
        next.classList.remove("untyped", "next", "good", "bad");
        next.classList.add("untyped");
    }

    reset() {
        this.at = -1;
        this.text.innerHTML = null;
        this.asSpans();
    }
}
